package dev.tacon.jakartaee.validation;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class TemporalRangeValidatorForZonedDateTime extends TemporalRangeValidator<ZonedDateTime, ZonedDateTime> {

	@Override
	protected ZonedDateTime convert(final ZonedDateTime value) {
		return value;
	}

	@Override
	protected ZonedDateTime getCurrent(final ChronoUnit unit) {
		return ZonedDateTime.now();
	}

	@Override
	protected ZonedDateTime calculateMin(final ZonedDateTime current, final int units, final ChronoUnit unit) {
		return current.minus(units, unit);
	}

	@Override
	protected ZonedDateTime calculateMax(final ZonedDateTime current, final int units, final ChronoUnit unit) {
		return current.plus(units, unit);
	}
}

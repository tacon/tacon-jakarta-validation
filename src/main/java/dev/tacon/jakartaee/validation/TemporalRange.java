package dev.tacon.jakartaee.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.temporal.ChronoUnit;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

/**
 * Annotation used to validate that a given temporal value falls within a specific range.
 * <p>
 * Supported types are:
 * <ul>
 * <li>{@code java.util.Date}</li>
 * <li>{@code java.time.Instant}</li>
 * <li>{@code java.time.LocalDate}</li>
 * <li>{@code java.time.LocalDateTime}</li>
 * <li>{@code java.time.LocalTime}</li>
 * <li>{@code java.time.OffsetDateTime}</li>
 * <li>{@code java.time.ZonedDateTime}</li>
 * </ul>
 * <p>
 */
@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {
		TemporalRangeValidatorForDate.class,
		TemporalRangeValidatorForInstant.class,
		TemporalRangeValidatorForLocalDate.class,
		TemporalRangeValidatorForLocalTime.class,
		TemporalRangeValidatorForLocalDateTime.class,
		TemporalRangeValidatorForOffsetDateTime.class,
		TemporalRangeValidatorForZonedDateTime.class
})
@Documented
public @interface TemporalRange {

	String message() default "{dev.tacon.jakartaee.validation.TemporalRange.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	/**
	 * Specifies how many units back the temporal value should be considered valid.
	 * A negative integer means unbounded.
	 *
	 * @return the number of units back. Default is {@code -1} (unbounded).
	 */
	int back() default -1;

	/**
	 * Specifies how many units forward the temporal value should be considered valid.
	 * A negative integer means unbounded.
	 *
	 * @return the number of units forward. Default is {@code -1} (unbounded).
	 */
	int forward() default -1;

	/**
	 * Specifies the unit of time to use when calculating the range.
	 *
	 * @return the {@code ChronoUnit} to be used. Default is {@linkplain ChronoUnit#DAYS}.
	 */
	ChronoUnit unit() default ChronoUnit.DAYS;
}
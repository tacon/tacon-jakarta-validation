package dev.tacon.jakartaee.validation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

class TemporalRangeTest {

	private static ValidatorFactory validatorFactory;
	private static Validator validator;

	@BeforeAll
	public static void setUpValidator() {
		validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}

	@AfterAll
	public static void terminate() {
		validatorFactory.close();
	}

	@Test
	void testTemporalRangeValid() {
		final SampleEntity entity = new SampleEntity(LocalDate.now().minus(3, ChronoUnit.DAYS));
		final Set<ConstraintViolation<SampleEntity>> violations = validator.validate(entity);
		assertTrue(violations.isEmpty());
	}

	@Test
	void testTemporalRangeInvalidPast() {
		final SampleEntity entity = new SampleEntity(LocalDate.now().minus(6, ChronoUnit.DAYS));
		final Set<ConstraintViolation<SampleEntity>> violations = validator.validate(entity);
		assertFalse(violations.isEmpty());
	}

	@Test
	void testTemporalRangeInvalidFuture() {
		final SampleEntity entity = new SampleEntity(LocalDate.now().plus(6, ChronoUnit.DAYS));
		final Set<ConstraintViolation<SampleEntity>> violations = validator.validate(entity);
		assertFalse(violations.isEmpty());
	}
}